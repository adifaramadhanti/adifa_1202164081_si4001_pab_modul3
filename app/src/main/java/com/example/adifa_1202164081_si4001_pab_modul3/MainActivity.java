package com.example.adifa_1202164081_si4001_pab_modul3;

import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {
    Dialog dialog;
    private RecyclerView mRecyclerView;
    private ArrayList<User> daftarUser;
    private AdapterUntukUser adapterUntukUser;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        mRecyclerView = findViewById(R.id.idRecyclerView);

        /*GridColumn buat ngatur jumlah kolomnya,
        */
        int gridColumnCount =getResources().getInteger(R.integer.grid_column_count);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this,gridColumnCount));

        daftarUser = new ArrayList<>();
        //Mengecek ada data Array di savedInstance
        if (savedInstanceState!=null){
            //Jika tidak null Maka akan mengembalikan datanya
            daftarUser.clear();
            for (int i = 0; i <savedInstanceState.getStringArrayList("nama").size() ; i++) {
                daftarUser.add(new User(savedInstanceState.getStringArrayList("nama").get(i),
                        savedInstanceState.getStringArrayList("pekerjaan").get(i),
                        savedInstanceState.getIntegerArrayList("gender").get(i)));
            }
        }else {
            //kalau null doi bakalan balik dari awal
            init();
        }

        adapterUntukUser = new AdapterUntukUser(daftarUser,this);
        mRecyclerView.setAdapter(adapterUntukUser);

        //ItemTouchHelper biar bisa di swipe
        ItemTouchHelper helper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.DOWN | ItemTouchHelper.UP,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                int from = viewHolder.getAdapterPosition();
                int to = target.getAdapterPosition();
                Collections.swap(daftarUser, from ,to);
                adapterUntukUser.notifyItemMoved(from,to);

                return true;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                daftarUser.remove(viewHolder.getAdapterPosition());
                adapterUntukUser.notifyItemRemoved(viewHolder.getAdapterPosition());
            }
        });
        //helper masuk ke RecyclerView
        helper.attachToRecyclerView(mRecyclerView);

    }

    void init(){
        //Masukkan Data Dummy
        daftarUser.clear();
        //Datanya
        daftarUser.add(new User("John","Android Dev",1));
        daftarUser.add(new User("Jessy","Front End",2));

    }

    //untuk menambah pop up user
    void tambah(View view){
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.add_dialog);
        final TextView mNama,mPekerjaan;
        final Spinner mGender;
        mNama = dialog.findViewById(R.id.tx_detail_Nama);
        mPekerjaan = dialog.findViewById(R.id.tx_detail_Pekerjaan);

        TextView tambah=dialog.findViewById(R.id.txTambahUser);
        TextView batal = dialog.findViewById(R.id.txBatal);

        mGender = dialog.findViewById(R.id.gender);

        String[]list={"Male","Female"};

        ArrayAdapter<String>adapterX = new ArrayAdapter(dialog.getContext(),android.R.layout.simple_spinner_item,list);
        mGender.setAdapter(adapterX);

        tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                daftarUser.add(new User(mNama.getText().toString(),mPekerjaan.getText().toString(),mGender.getSelectedItemPosition()+1));
                adapterUntukUser.notifyDataSetChanged();
                dialog.dismiss();
            }
        });
        batal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        //Karena ribet saveInstance ArrayAdapter sehingga harus di breakdown lagi menjadi arrayList
        //convert menjadi arraylist
        ArrayList<String> tempListNama = new ArrayList<>();
        ArrayList<String>tempListPekerjaan = new ArrayList<>();
        ArrayList<Integer>tempListGender = new ArrayList<>();
        for (int i = 0; i <daftarUser.size() ; i++) {
            tempListNama.add(daftarUser.get(i).getNama());
            tempListPekerjaan.add(daftarUser.get(i).getPekerjaan());
            tempListGender.add(daftarUser.get(i).getAvatar());
        }

        //outsite disimpan
        outState.putStringArrayList("nama",tempListNama);
        outState.putStringArrayList("pekerjaan",tempListPekerjaan);
        outState.putIntegerArrayList("gender",tempListGender);
        super.onSaveInstanceState(outState);

    }
}
